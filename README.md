* We try to compress reinforcement learning policies into more interpretable models, like decision trees. The process is as follows:

    1. Train a deep Q-network agent until convergence on a certain environment.
    2. Freeze the neural networks parameters, and let the agent play out a certain number of episodes on the environment.
    3. Save the states (inputs) and Q-values (outputs) it obtains thereby effectively forming a dataset of experiences.
    4. Use symbolic regression strategies to produce a range of interpretable decision trees that fit the dataset. 
    5. Plug the interpretable agent into the environment, and benchmark its performance. 
    6. Iterate and benchmark different models from the set of models until one fits the requirements of interpretability/performance.